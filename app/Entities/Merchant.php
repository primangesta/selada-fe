<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Merchants.
 *
 * @package namespace App\Entities;
 */
class Merchant extends Model implements Transformable
{
    use TransformableTrait;
    use SoftDeletes;

    public $incrementing = true;

    protected $fillable = [
        'id',
        'merchant_id',
        'type',
        'name',
        'no',
        'code',
        'address',
        'phone',
        'email',
        'balance',
        'avatar',
        'status',
        'user_id'
    ];

    protected $table = 'merchants';
}
