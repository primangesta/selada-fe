<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    protected function redirectTo($request)
    {
        // if ( ! $this->auth->user() )
        // {
        //     // here you should redirect to login 
        //     return view('sessions.signIn');
        // }
        if (! $this->auth->user()) {
            return route('login');
        }
    }
}
