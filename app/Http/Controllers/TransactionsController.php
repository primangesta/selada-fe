<?php
// Connect to ARDI

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use DB;
use Carbon\Carbon;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\TransactionCreateRequest;
use App\Http\Requests\TransactionUpdateRequest;
use App\Repositories\TransactionRepository;
use App\Validators\TransactionValidator;
use Ixudra\Curl\Facades\Curl;

use App\Entities\Merchant;
use App\Entities\Service;
use App\Entities\Transaction;
use App\Entities\TransactionStatus;
use App\Entities\transactionPaymentStatus;

use App\Http\Controllers\CoresController as Core;

/**
 * Class TransactionsController.
 *
 * @package namespace App\Http\Controllers;
 */
class TransactionsController extends Controller
{
    /**
     * @var TransactionRepository
     */
    protected $repository;

    /**
     * @var TransactionValidator
     */
    protected $validator;

    /**
     * TransactionsController constructor.
     *
     * @param TransactionRepository $repository
     * @param TransactionValidator $validator
     */
    public function __construct(TransactionRepository $repository, TransactionValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));

        $data = Transaction::select('*');

        if($request->has('search')){
            $data = $data->where('code','iLIKE',"%{$request->search}%");
        }

        if($request->has('merchant_id')){
            $data = $data->where('merchant_id',$request->merchant_id);
        }

        if($request->has('status')){
            $data = $data->where('status',$request->status);
        }

        if($request->has('payment_status')){
            $data = $data->where('payment_status',$request->payment_status);
        }

        if($request->has('start_date')){
            $data = $data->where('created_at','>=',$request->start_date);
        }

        if($request->has('end_date')){
            $data = $data->where('created_at','<=',$request->end_date);
        }

        $total = $data->count();
    
        if($request->has('limit')){
            $data->take($request->get('limit'));
            
            if($request->has('offset')){
            	$data->skip($request->get('offset'));
            }
        }

        if($request->has('order_type')){
            if($request->get('order_type') == 'asc'){
                if($request->has('order_by')){
                    $data->orderBy($request->get('order_by'));
                }else{
                    $data->orderBy('created_at');
                }
            }else{
                if($request->has('order_by')){
                    $data->orderBy($request->get('order_by'), 'desc');
                }else{
                    $data->orderBy('created_at', 'desc');
                }
            }
        }else{
            $data->orderBy('created_at', 'desc');
        }

        $data = $data->with(['merchant','service.product.provider.category','transactionStatus','transactionPaymentStatus']);

        $data = $data->get();

        foreach($data as $item){
            if($item->status == 0){
                $item->status_text = 'Pending';
            }

            if($item->status == 1){
                $item->status_text = 'Success';
            }

            if($item->status == 2){
                $item->status_text = 'Failed';
            }
        }

        return view('apps.transactions.list')
                ->with('data', $data);
    }
}
