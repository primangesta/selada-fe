<!-- Footer Start -->
<div class="flex-grow-1"></div>
<div class="app-footer p-3">
    <div class="row">
        
    </div>
    <div class="footer-bottom d-flex flex-column flex-sm-row align-items-center ">
    <div class="logo_bintang_footer">
                <img src="{{asset('/assets/images/icon_bintang/selada-transparant-final.png')}}" alt="">
            </div>
        <span class="flex-grow-1"></span>
        <div class="d-flex align-items-center">
           
            <div class="flex-r">
                <p class="m-0">&copy; 2019 Selada</p>
                <p class="m-0 float-right">All rights reserved</p>
            </div>
        </div>
    </div>
</div>
<!-- fotter end -->