<div class="side-content-wrap">
    <div class="sidebar-left open rtl-ps-none" data-perfect-scrollbar data-suppress-scroll-x="true">
        <ul class="navigation-left">
            <li class="nav-item {{ request()->is('dashboard/*') ? 'active' : '' }}">
                <a class="nav-item-hold" href="{{route('dashboard_version_1')}}">
                <img class="img_bintang_sidebar" src="{{asset('/assets/images/icon_bintang/dashboard.png')}}" alt="">
                    <span class="nav-text">Dashboard</span>
                </a>
                <div class="triangle"></div>
            </li>
            <li class="nav-item {{ request()->is('uikits/*') ? 'active' : '' }}">
                <a class="nav-item-hold" href="{{route('dashboard_version_4')}}">
                 
                    <img class="img_bintang_sidebar" src="{{asset('/assets/images/icon_bintang/agent.png')}}" alt="">
                    <span class="nav-text">Agent</span>
                </a>
                <div class="triangle"></div>
            </li>
            <li class="nav-item {{ request()->is('extrakits/*') ? 'active' : '' }}">
                <a class="nav-item-hold" href="{{route('transaction')}}">
                 
                    <img class="img_bintang_sidebar" src="{{asset('/assets/images/icon_bintang/transaction.png')}}"alt="">
                    <span class="nav-text">Transaction</span>
                </a>
                <div class="triangle"></div>
            </li>
            <!-- <li class="nav-item {{ request()->is('extrakits/*') ? 'active' : '' }}">
                <a class="nav-item-hold" href="{{route('dashboard_version_7')}}">
                 
                    <img class="img_bintang_sidebar" src="{{asset('/assets/images/icon_bintang/revenue.png')}}" alt="">
                    <span class="nav-text">Revenue</span>
                </a>
                <div class="triangle"></div>
            </li> -->
            <!-- <li class="nav-item {{ request()->is('extrakits/*') ? 'active' : '' }}">
                <a class="nav-item-hold" href="{{route('dashboard_version_10')}}">
                 
                    <img class="img_bintang_sidebar" src="{{asset('/assets/images/icon_bintang/topup.png')}}" alt="">
                    <span class="nav-text">Topup</span>
                </a>
                <div class="triangle"></div>
            </li> -->
            <li class="nav-item {{ request()->is('extrakits/*') ? 'active' : '' }}">
                <a class="nav-item-hold" href="{{route('biller')}}">
                 
                    <img class="img_bintang_sidebar" src="{{asset('/assets/images/icon_bintang/biller.png')}}" alt="">
                    <span class="nav-text">Biller</span>
                </a>
                <div class="triangle"></div>
            </li>
            <li class="nav-item {{ request()->is('extrakits/*') ? 'active' : '' }}">
                <a class="nav-item-hold" href="{{route('dashboard_version_11')}}">
                 
                    <img class="img_bintang_sidebar" src="{{asset('/assets/images/icon_bintang/version.png')}}" alt="">
                    <span class="nav-text">Terminal</span>
                </a>
                <div class="triangle"></div>
            </li>
            <li class="nav-item {{ request()->is('extrakits/*') ? 'active' : '' }}">
                <a class="nav-item-hold" href="{{route('dashboard_version_11')}}">
                 
                    <img class="img_bintang_sidebar" src="{{asset('/assets/images/icon_bintang/version.png')}}" alt="">
                    <span class="nav-text">Version</span>
                </a>
                <div class="triangle"></div>
            </li>
          
        </ul>
    </div>

    <div class="sidebar-left-secondary rtl-ps-none" data-perfect-scrollbar data-suppress-scroll-x="true">
        <!-- Submenu Dashboards -->
        <ul class="childNav" data-parent="Transaksi">
            <li class="nav-item ">
                <a class="{{ Route::currentRouteName()=='dashboard_version_1' ? 'open' : '' }}"
                    href="{{route('dashboard_version_1')}}">
                    <i class="nav-icon i-Clock-3"></i>
                    <span class="item-name">Distributor Utama</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{route('dashboard_version_2')}}"
                    class="{{ Route::currentRouteName()=='dashboard_version_2' ? 'open' : '' }}">
                    <i class="nav-icon i-Clock-4"></i>
                    <span class="item-name">Distributor</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="{{ Route::currentRouteName()=='dashboard_version_3' ? 'open' : '' }}"
                    href="{{route('dashboard_version_3')}}">
                    <i class="nav-icon i-Over-Time"></i>
                    <span class="item-name">Agen</span>
                </a>
            </li>
           
        </ul>
    </div>
    <div class="sidebar-overlay"></div>
</div>
<!--=============== Left side End ================-->