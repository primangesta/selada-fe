@extends('layouts.master')

@section('page-css')
 <link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
 <link rel="stylesheet" href="{{asset('assets/styles/custom/bintang.css')}}">

@endsection
@section('main-content')
       <div class="breadcrumb">
                <h1>Revenue</h1>
                <ul>
                    <li><a href="">Selada</a></li>
                </ul>
            </div>
            <div class="separator-breadcrumb border-top"></div>
            <div class="row mb-4">


                    <div class="col-md-12 mb-3">
                        <div class="card text-left">

                            <div class="card-body">
                            <div class="flex_bintang">
                                <h4 class="card-title mb-3">List Toko </h4>
                               
                            </div>
                                <div class="table-responsive">
                                    <table class="table">

                                        <thead>
                                            <tr>
                                                <th scope="col">Nomor</th>
                                                <th scope="col">Nama Toko</th>
                                                <th scope="col">Alamat</th>
                                                <th scope="col">Pendapatan</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <th scope="row">1</th>
                                                <td> Imam Ahli Jaya</td>
                                                <td>

                                                    Jl Insinyur artos

                                                </td>

                                                <td><span class="badge_bintang2">Rp.1.500.000.000</span></td>
                                              
                                            </tr>
                                            <tr>
                                                <th scope="row">1</th>
                                                <td> Imam Ahli Jaya</td>
                                                <td>

                                                    Jl Insinyur artos2

                                                </td>

                                                <td> <span class="badge_bintang2">Rp.1.500.000.000</span></td>
                                              
                                            </tr>
                                            <tr>
                                                <th scope="row">1</th>
                                                <td> Imam Ahli Jaya</td>
                                                <td>

                                                    Jl Insinyur artos

                                                </td>

                                                <td><span class="badge_bintang2">Rp.1.500.000.000</span></td>
                                              
                                            </tr>
                                         
                                        

                                        </tbody>
                                    </table>
                                </div>


                            </div>
                        </div>
                    </div>


        </div>
           
@endsection

@section('page-js')
     <script src="{{asset('assets/js/vendor/echarts.min.js')}}"></script>
     <script src="{{asset('assets/js/es5/echart.options.min.js')}}"></script>
      <script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
     <script src="{{asset('assets/js/es5/dashboard.v2.script.js')}}"></script>

@endsection
